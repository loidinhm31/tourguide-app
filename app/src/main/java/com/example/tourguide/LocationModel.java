package com.example.tourguide;

public class LocationModel {
    private String name;
    private String address;

    public LocationModel(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }



    @Override
    public String toString() {
        return String.format("%s\n%s", name, address);
    }
}
