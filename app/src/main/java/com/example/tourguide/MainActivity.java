package com.example.tourguide;




import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;


import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.tourguide.ui.AtmFragment;
import com.example.tourguide.ui.HospitalFragment;
import com.example.tourguide.ui.HotelFragment;
import com.example.tourguide.ui.MenuFragment;
import com.example.tourguide.ui.MetroFragment;


public class MainActivity extends FragmentActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        if (findViewById(R.id.fragment_container) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }


            MenuFragment menuFragment = new MenuFragment();

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, menuFragment).commit();
        }
    }

}