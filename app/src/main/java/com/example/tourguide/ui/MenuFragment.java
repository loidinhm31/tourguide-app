package com.example.tourguide.ui;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;



import androidx.fragment.app.Fragment;

import androidx.fragment.app.FragmentTransaction;

import com.example.tourguide.R;


public class MenuFragment extends Fragment {

    public MenuFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle SavedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu, container, false);


        // Create Hotel Click
        ImageView hotelView = view.findViewById(R.id.hotel_img);
        hotelView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Create fragment
                HotelFragment hotelFragment = new HotelFragment();

                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack so the user can navigate back
                transaction.replace(R.id.fragment_container, hotelFragment);
                transaction.addToBackStack(null);

                // Commit the transaction
                transaction.commit();
            }
        });

        // Create ATM Click
        ImageView atmView = view.findViewById(R.id.atm_img);
        atmView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Create Fragment
                AtmFragment atmFragment = new AtmFragment();

                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack so the user can navigate back
                transaction.replace(R.id.fragment_container, atmFragment);
                transaction.addToBackStack(null);

                // Commit the transaction
                transaction.commit();
            }
        });


        // Create Hospital Click
        ImageView hospitalView = view.findViewById(R.id.hospital_img);
        hospitalView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Create Fragment
                HospitalFragment hospitalFragment = new HospitalFragment();

                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack so the user can navigate back
                transaction.replace(R.id.fragment_container, hospitalFragment);
                transaction.addToBackStack(null);

                // Commit transaction
                transaction.commit();
            }
        });

        // Create Metro Click
        ImageView metroView = view.findViewById(R.id.metro_img);
        metroView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MetroFragment metroFragment = new MetroFragment();

                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack so the user can navigate back
                transaction.replace(R.id.fragment_container, metroFragment);
                transaction.addToBackStack(null);

                // Commit transaction
                transaction.commit();
            }
        });

        return view;
    }



}
