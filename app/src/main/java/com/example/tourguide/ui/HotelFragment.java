package com.example.tourguide.ui;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.example.tourguide.LocationAdapter;
import com.example.tourguide.LocationModel;
import com.example.tourguide.R;

import java.util.ArrayList;

public class HotelFragment extends Fragment {
    public HotelFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle SavedInstanceState) {
        // Create View
        View view = inflater.inflate(R.layout.fragment_item, container, false);

        // Create a List of Hotels
        final ArrayList<LocationModel> hotels = new ArrayList<>();

        // Add location object ot list
        hotels.add(new LocationModel("The Queen Hotel & Spa","67 Thuốc Bắc, Hàng Bồ, Hàng Bồ, Quận Hoàn Kiếm, Hà Nội, Việt Nam"));
        hotels.add(new LocationModel("Hanoi Nostalgia Hotel & Spa", "13-15 Luong Ngoc Quyen, Hang Buom, Hoan Kiem, Hàng Buồm, Quận Hoàn Kiếm, Hà Nội, Việt Nam"));
        hotels.add(new LocationModel("Church Legend Hotel Hanoi", "46 Ấu Triệu, Phường Hàng Trống, Quận Hoàn Kiếm, Hà Nội, Việt Nam"));
        hotels.add(new LocationModel("Little Hanoi Diamond Hotel", "11 Bát Đàn, Quận Hoàn Kiếm, Hàng Bồ, Quận Hoàn Kiếm, Hà Nội, Việt Nam"));
        hotels.add(new LocationModel("Flamingo Dai Lai Resort", "Thôn Ngọc Quang, Xã Ngọc Thanh, Vĩnh Phúc, Phúc Yên, Hà Nội, Việt Nam"));
        hotels.add(new LocationModel("Annam Legend Hotel", "27 Hàng Bè, Hàng Bạc, Quận Hoàn Kiếm, Hà Nội, Việt Nam"));
        hotels.add(new LocationModel("Hanoi Zesty Hotel", "20 Hàng Cân, Hàng Đào, Quận Hoàn Kiếm, Hà Nội, Việt Nam"));
        hotels.add(new LocationModel("Bluebell Hotel", "41 Ngõ Huyện, Phường Hàng Trống, Quận Hoàn Kiếm, Hà Nội, Việt Nam"));


        // Create a icon list to fit with locations
        int[] icons = new int[hotels.size()];

        // Add corresponding icon with item
        for (int i = 0; i < icons.length; i++) {
            icons[i] = R.drawable.hotel;
        }

        // Create Adapter
        LocationAdapter adapter = new LocationAdapter(getActivity(), hotels, icons);

        // Create ListView
        ListView listView = view.findViewById(R.id.list_location);
        listView.setAdapter(adapter);

        //Create Listener when clicking on an item to search on Google Maps
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                String address = hotels.get(position).getAddress();

                Toast.makeText(getContext(), "Searching for: " + address, Toast.LENGTH_SHORT).show();

                // Searching location on Google Maps
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("geo:0,0?q=" + address));

                if (intent != null) {
                    startActivity(intent);
                }
            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Set title
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar2);
        TextView title = toolbar.findViewById(R.id.title2);
        title.setText("Khách sạn");

        // Add Back Button
        ImageView backBtn = getActivity().findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HotelFragment.super.getActivity().onBackPressed();
            }
        });
    }


}





