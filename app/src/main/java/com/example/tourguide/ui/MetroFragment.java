package com.example.tourguide.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.example.tourguide.LocationAdapter;
import com.example.tourguide.LocationModel;
import com.example.tourguide.R;

import java.util.ArrayList;

public class MetroFragment extends Fragment {
    public MetroFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Create View
        View view = inflater.inflate(R.layout.fragment_item, container, false);

        // Create a List of Metro
        final ArrayList<LocationModel> metros = new ArrayList<>();

        // Add location object to list
        metros.add(new LocationModel("Tuyến 01", "BX Gia Lâm - BX Yên Nghĩa"));
        metros.add(new LocationModel("Tuyến 02", "Bác cổ - BX Yên Nghĩa"));
        metros.add(new LocationModel("Tuyến 03A", "BX Giáp Bát - BX Gia Lâm"));
        metros.add(new LocationModel("Tuyến 03B", "Bx Giáp Bát - Vincom - Phúc Lợi"));
        metros.add(new LocationModel("Tuyến 04", "Long Biên - BX Nước Ngầm"));
        metros.add(new LocationModel("Tuyến 05", "Linh Đàm - Phú Diễn"));
        metros.add(new LocationModel("Tuyến 06", "BX. Giáp Bát - Phú Minh (Phú Xuyên)"));
        metros.add(new LocationModel("Tuyến 07", "Cầu Giấy - Nội Bài"));
        metros.add(new LocationModel("Tuyến 08", "Long Biên - Đông Mỹ"));

        // Create a icon list to fit with locations
        int[] icons = new int[metros.size()];

        // Add corresponding icon with item
        for (int i = 0; i < icons.length; i++) {
            icons[i] = R.drawable.metro;
        }

        // Create Adapter
        LocationAdapter adapter = new LocationAdapter(getActivity(), metros, icons);

        // Create ListView
        ListView listView = view.findViewById(R.id.list_location);
        listView.setAdapter(adapter);

        //Create Listener when clicking on an item to search on Google Maps
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                String address = metros.get(position).getAddress();

                Toast.makeText(getContext(), "Searching for: " + address, Toast.LENGTH_SHORT).show();

                // Searching location on Google Maps
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("geo:0,0?q=" + address));

                if (intent != null) {
                    startActivity(intent);
                }
            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Set Title
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar2);
        TextView title = toolbar.findViewById(R.id.title2);
        title.setText("Xe bus");

        // Add Back Button
        ImageView backBtn = getActivity().findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MetroFragment.super.getActivity().onBackPressed();
            }
        });

    }
}
