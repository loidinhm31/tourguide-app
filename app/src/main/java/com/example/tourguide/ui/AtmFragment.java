package com.example.tourguide.ui;


import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.example.tourguide.LocationAdapter;
import com.example.tourguide.LocationModel;
import com.example.tourguide.R;

import java.util.ArrayList;

public class AtmFragment extends Fragment {

    public AtmFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Create View
        View view = inflater.inflate(R.layout.fragment_item, container, false);


        // Create a List of ATMs
        final ArrayList<LocationModel> atms = new ArrayList<>();

        // Add location object to list
        atms.add(new LocationModel("ATM Hoàn Kiếm", "17 phố Lý Thường Kiệt, Phường Phan Chu Trinh, Quận Hoàn Kiếm, Hà Nội"));
        atms.add(new LocationModel("ATM Đinh Tiên Hoàng", "7 Đinh Tiên Hoàng, Quận Hoàn Kiếm, Hà Nội"));
        atms.add(new LocationModel("ATM Hội sở", "57 Lý Thường Kiệt, Quận Hoàn Kiếm, Hà Nội"));
        atms.add(new LocationModel("ATM Nam Hà Nội", "236 Lê Thanh Nghị, Quận Hai Bà Trưng, Hà Nội"));
        atms.add(new LocationModel("ATM Hai Bà Trưng","300-302 Trần Khát Chân, Quận Hai Bà Trưng, Hà Nội"));
        atms.add(new LocationModel("ATM Lê Ngọc Hân", "44 Lê Ngọc Hân, Quận Hai Bà Trưng, Hà Nội"));
        atms.add(new LocationModel("ATM Thăng Long", "129-131 Hoàng Quốc Việt, Quận Cầu Giấy, Hà Nội"));
        atms.add(new LocationModel("ATM Phạm Hùng", "Tòa nhà FPT Phạm Hùng, Quận Cầu Giấy, Hà Nội"));
        atms.add(new LocationModel("ATM Khâm Thiên","158 Khâm Thiên, Quận Đống Đa, Hà Nội"));

        // Create a icon list to fit with locations
        int[] icons = new int[atms.size()];

        // Add corresponding icon with item
        for (int i = 0; i < icons.length; i++) {
            icons[i] = R.drawable.atm_machine;
        }

        // Create Adapter
        LocationAdapter adapter = new LocationAdapter(getActivity(), atms, icons);

        // Create ListView
        final ListView listView = view.findViewById(R.id.list_location);
        listView.setAdapter(adapter);

        // Create Listener when clicking on an item to search on Google Maps
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                String address = atms.get(position).getAddress();

                Toast.makeText(getContext(), "Searching for: " + address, Toast.LENGTH_SHORT).show();

                // Searching location on Google Maps
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("geo:0,0?q=" + address));

                if (intent != null) {
                    startActivity(intent);
                }
            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Set title
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar2);
        TextView title = toolbar.findViewById(R.id.title2);
        title.setText("ATM");


        // Add Back Button
        ImageView backBtn = getActivity().findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AtmFragment.super.getActivity().onBackPressed();
            }
        });
    }

}
