package com.example.tourguide.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.example.tourguide.LocationAdapter;
import com.example.tourguide.LocationModel;
import com.example.tourguide.R;

import java.util.ArrayList;


public class HospitalFragment extends Fragment {
    public HospitalFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Create View
        View view = inflater.inflate(R.layout.fragment_item, container, false);

        // Create a List of Hospitals
        final ArrayList<LocationModel> hospitals = new ArrayList<>();

        // Add location object to list
        hospitals.add(new LocationModel("Bệnh viện Bạch Mai", "78 – Đường Giải Phóng – Phương Mai – Đống Đa – Hà Nội"));
        hospitals.add(new LocationModel("Bệnh Viện Hữu Nghị", "Số 1 – Trần Khánh Dư – Quận Hai Bà Trưng – Hà Nội"));
        hospitals.add(new LocationModel("Bệnh Viện E, Hà Nội",
                "89 – Trần Cung – Nghĩa Tân – Cầu Giấy – Hà Nội"));
        hospitals.add(new LocationModel("Viện Răng Hàm Mặt", "40B – Tràng Thi – Hoàn Kiếm – Hà Nội"));
        hospitals.add(new LocationModel("Bệnh Viện Tai Mũi Họng Trung Ương", "78 – Đường Giải Phóng – Quận Đống Đa – Hà Nội"));
        hospitals.add(new LocationModel("Bệnh Viện Mắt Trung Ương", "85 – Phố Bà Triệu – Quận Hai Bà Trưng – Hà Nội"));
        hospitals.add(new LocationModel("Viện Y Học Cổ Truyền Trung Ương", "29 – Phố Nguyễn Bỉnh Khiêm – Quận Hai Bà Trưng – Hà Nội"));
        hospitals.add(new LocationModel("Bệnh Viện Nội Tiết", "80 – Thái Thịnh II – Thịnh Quang – Đống Đa – Hà Nội"));
        hospitals.add(new LocationModel("Bệnh Viện Việt Đức", "8 – Phố Phủ Doãn – Quận Hoàn Kiếm – Hà Nội"));
        hospitals.add(new LocationModel("Bệnh Viện Nhi Trung Ương", "18/879 – Đường La Thành – Quận Đống Đa – Hà Nội"));

        // Create a icon list to fit with locations
        int[] icons = new int[hospitals.size()];

        // Add corresponding icon with item
        for (int i = 0; i < icons.length; i++) {
            icons[i] = R.drawable.hospital;
        }

        // Create Adapter
        LocationAdapter adapter = new LocationAdapter(getActivity(), hospitals, icons);

        // Create ListView
        ListView listView = view.findViewById(R.id.list_location);
        listView.setAdapter(adapter);

        //Create Listener when clicking on an item to search on Google Maps
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                String address = hospitals.get(position).getAddress();

                Toast.makeText(getContext(), "Searching for: " + address, Toast.LENGTH_SHORT).show();

                // Searching location on Google Maps
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("geo:0,0?q=" + address));

                if (intent != null) {
                    startActivity(intent);
                }
            }
        });



        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Set Title
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar2);
        TextView title = toolbar.findViewById(R.id.title2);
        title.setText("Bệnh viện");

        // Add Back Button
        ImageView backBtn = getActivity().findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HospitalFragment.super.getActivity().onBackPressed();
            }
        });
    }
}
