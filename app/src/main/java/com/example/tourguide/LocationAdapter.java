package com.example.tourguide;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;


public class LocationAdapter extends ArrayAdapter<LocationModel> {
    private Context mContext;

    private ArrayList<LocationModel> mLocationData;

    private int[] mIcons;

    private LayoutInflater mInflater;



    public LocationAdapter(Context context, ArrayList<LocationModel> locationData, int[] icons) {
        super(context, 0, locationData);
        this.mContext = context;
        this.mLocationData = locationData;
        this.mIcons = icons;
        mInflater = (LayoutInflater.from(mContext));
    }


    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        // Create View with "row_item" template
        ViewHolder viewHolder;
        if (view == null) {
            view = mInflater.inflate(R.layout.row_item, null);

            //Retrieve the view on the item layout and set the value
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        // Retrieve object
        viewHolder.location.setText(mLocationData.get(position).toString());
        viewHolder.icon.setImageResource(mIcons[position]);

        return view;
    }

    public class ViewHolder {
        private final TextView location;
        private final ImageView icon;

        private  ViewHolder (View view) {
            location = view.findViewById(R.id.text);
            icon = view.findViewById(R.id.icon);

        }


    }


    public LocationModel getItem(int position) {
        return mLocationData.get(position);
    }


}
